<?php

namespace App\Controller;

use App\Entity\Biblioteca;
use App\Form\BibliotecaFormType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Event\Message;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends AbstractController
{
    /**
     * @Route("/create", name="app_create")
     */
    public function index(Request $request, ManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(BibliotecaFormType::class, new Biblioteca());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $articulo = $form->getData();
            $em = $doctrine->getManager();
            $em->persist($articulo);
            $em->flush();
            
            return $this->redirectToRoute('app_list');
        }


        return $this->render('create/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
