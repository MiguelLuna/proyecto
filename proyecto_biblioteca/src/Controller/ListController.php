<?php

namespace App\Controller;

use App\Entity\Biblioteca;
use App\Repository\BibliotecaRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    /**
     * @Route("/list", name="app_list")
     */
    public function index(BibliotecaRepository $bibliotecaRepository): Response
    {
        return $this->render('list/index.html.twig', [
            'libros' => $bibliotecaRepository->findAll(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="app_delete")
     */
    public function delete(Biblioteca $biblioteca, ManagerRegistry $doctrine): RedirectResponse
    {
        $em = $doctrine->getManager();
        $em->remove($biblioteca);
        $em->flush();
        return $this->redirectToRoute('app_list');
    }
}
