<?php

namespace App\Controller;

use App\Entity\Biblioteca;
use App\Form\BibliotecaFormType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    /**
     * @Route("/edit/{id}", name="app_edit")
     */
    public function index(Biblioteca $biblioteca, Request $request, ManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(BibliotecaFormType::class, $biblioteca);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $libro = $form->getData();
            $em = $doctrine->getManager();
            $em->persist($libro);
            $em->flush();

            return $this->redirectToRoute('app_list');
        
        };
        return $this->render('create/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
