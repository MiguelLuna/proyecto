<?php

namespace App\Form;

use App\Entity\Biblioteca;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BibliotecaFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /*$builder
            ->add('Titulo')
            ->add('Breve_descripcion')
            ->add('Anio_publicacion')
            ->add('Autor')
        ;*/
        $builder->add('Titulo', Texttype::class, ['attr' => ['class' => 'form-control']]);
        $builder->add('Breve_descripcion', TextType::class, ['attr' => ['class' => 'form-control']]);
        $builder->add('Anio_publicacion', NumberType::class, ['attr' => ['class' => 'form-control']]);
        $builder->add('Autor', Texttype::class, ['attr' => ['class' => 'form-control']]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Biblioteca::class,
        ]);
    }
}
