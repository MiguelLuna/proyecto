<?php

namespace App\DataFixtures;

use App\Entity\Biblioteca;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        for ($i=0; $i < 5; $i++) { 
            $biblioteca = new Biblioteca();
            $biblioteca->setTitulo("Titulo ".strval($i));
            $biblioteca->setBreveDescripcion("Este libro tiene una breve descripcion ".strval($i));
            $biblioteca->setAnioPublicacion(random_int(1800,1900));
            $biblioteca->setAutor("El autor del libro es: ".strval($i));
            $manager->persist($biblioteca);
            $manager->flush();
        }

        $manager->flush();
    }
}
