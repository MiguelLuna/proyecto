<?php

namespace App\Entity;

use App\Repository\BibliotecaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BibliotecaRepository::class)
 */
class Biblioteca
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $Titulo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Breve_descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $Anio_publicacion;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Autor;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitulo(): ?string
    {
        return $this->Titulo;
    }

    public function setTitulo(string $Titulo): self
    {
        $this->Titulo = $Titulo;

        return $this;
    }

    public function getBreveDescripcion(): ?string
    {
        return $this->Breve_descripcion;
    }

    public function setBreveDescripcion(string $Breve_descripcion): self
    {
        $this->Breve_descripcion = $Breve_descripcion;

        return $this;
    }

    public function getAnioPublicacion(): ?int
    {
        return $this->Anio_publicacion;
    }

    public function setAnioPublicacion(int $Anio_publicacion): self
    {
        $this->Anio_publicacion = $Anio_publicacion;

        return $this;
    }

    public function getAutor(): ?string
    {
        return $this->Autor;
    }

    public function setAutor(string $Autor): self
    {
        $this->Autor = $Autor;

        return $this;
    }
}
